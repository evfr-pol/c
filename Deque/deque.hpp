#pragma once
#include <cmath>
#include <cstddef>
#include <iostream>
#include <iterator>
#include <memory>
#include <vector>

template <typename T, typename Allocator = std::allocator<T>>
class Deque {
 public:
  template <bool IsConst>
  class common_iterator;

  using const_iterator = common_iterator<true>;
  using iterator = common_iterator<false>;
  using reverse_iterator = std::reverse_iterator<iterator>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;
  using allocator_type = Allocator;
  using alloc_traits = std::allocator_traits<Allocator>;

  Deque()
      : vec_(std::vector<T*>(3, nullptr)),
        size_left_(kRazmerArray),
        size_right_(kRazmerArray * 2),
        capacity_(kRazmerArray * 3),
        alloc_(Allocator()) {}

  Deque(const Allocator& alloc)
      : vec_(std::vector<T*>(3, nullptr)),
        size_left_(kRazmerArray),
        size_right_(kRazmerArray * 2),
        capacity_(kRazmerArray * 3),
        alloc_(alloc) {}

  Deque(const Deque<T, Allocator>& deq)
      : vec_(std::vector<T*>(deq.vec_.size(), nullptr)),
        size_left_(deq.size_left_),
        size_right_(deq.size_right_),
        capacity_(deq.capacity_),
        alloc_(std::allocator_traits<decltype(deq.alloc_)>::
                   select_on_container_copy_construction(deq.alloc_)) {
    for (int j = size_left_ / kRazmerArray;
         j < capacity_ / kRazmerArray - size_right_ / kRazmerArray; ++j) {
      vec_[j] = alloc_traits::allocate(alloc_, kRazmerArray);
    }
    int i_container = size_left_ / kRazmerArray;
    int j_container = 0;
    try {
      if ((i_container ==
           capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1) ||
          deq.empty()) {
        for (int j = size_left_ % kRazmerArray;
             j < (kRazmerArray - size_right_ % kRazmerArray) % kRazmerArray;
             ++j) {
          j_container = j;
          alloc_traits::construct(alloc_, vec_[i_container] + j,
                                  deq.vec_[i_container][j]);
        }
      } else {
        for (int j = size_left_ % kRazmerArray; j < kRazmerArray; ++j) {
          j_container = j;
          alloc_traits::construct(alloc_, vec_[i_container] + j,
                                  deq.vec_[i_container][j]);
        }
        ++i_container;
        j_container = 0;
        for (; i_container <
               capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1;
             ++i_container) {
          for (int j = 0; j < kRazmerArray; ++j) {
            j_container = j;
            alloc_traits::construct(alloc_, vec_[i_container] + j,
                                    deq.vec_[i_container][j]);
          }
        }
        j_container = 0;
        for (int j = 0;
             j < (kRazmerArray - size_right_ % kRazmerArray) % kRazmerArray;
             ++j) {
          j_container = j;
          alloc_traits::construct(alloc_, vec_[i_container] + j,
                                  deq.vec_[i_container][j]);
        }
      }
    } catch (...) {  // catch exceptions from constructor of T
      pro_destroy(i_container, j_container);
      for (int i1 = size_left_ / kRazmerArray;
           i1 < capacity_ / kRazmerArray - size_right_ / kRazmerArray; ++i1) {
        alloc_traits::deallocate(alloc_, vec_[i1], kRazmerArray);
      }
      throw;
    }
  }

  void pro_destroy(int i_container, int j_container) {
    if (i_container != size_left_ / kRazmerArray) {
      destroy_elements(vec_, size_left_ / kRazmerArray,
                       size_left_ % kRazmerArray, kRazmerArray);
      for (int i1 = size_left_ / kRazmerArray + 1; i1 < i_container; ++i1) {
        destroy_elements(vec_, i1, 0, kRazmerArray);
      }
      destroy_elements(vec_, i_container, 0, j_container);
    } else {
      destroy_elements(vec_, i_container, size_left_ % kRazmerArray,
                       j_container);
    }
  }

  Deque(int n, const T& value, const Allocator& alloc = Allocator())
      : vec_(std::vector<T*>(
            ceil(static_cast<double>(n) / static_cast<double>(kRazmerArray)),
            nullptr)),
        size_left_(0),
        size_right_((kRazmerArray - n % kRazmerArray) % kRazmerArray),
        capacity_(
            ceil(static_cast<double>(n) / static_cast<double>(kRazmerArray)) *
            kRazmerArray),
        alloc_(alloc) {
    int i_container = 0;
    for (; i_container < capacity_ / kRazmerArray; ++i_container) {
      vec_[i_container] = alloc_traits::allocate(alloc_, kRazmerArray);
    }
    i_container = 0;
    int j_container = 0;
    try {
      if (0 == capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1) {
        for (int j = size_left_ % kRazmerArray;
             j < (kRazmerArray - size_right_ % kRazmerArray) % kRazmerArray;
             ++j) {
          j_container = j;
          alloc_traits::construct(alloc_, vec_[i_container] + j, value);
        }
      } else {
        for (; i_container <
               capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1;
             ++i_container) {
          for (int j = 0; j < kRazmerArray; ++j) {
            j_container = j;
            alloc_traits::construct(alloc_, vec_[i_container] + j, value);
          }
        }
        j_container = 0;
        for (int j = 0;
             j < (kRazmerArray - size_right_ % kRazmerArray) % kRazmerArray;
             ++j) {
          j_container = j;
          alloc_traits::construct(alloc_, vec_[i_container] + j, value);
        }
      }
    } catch (...) {
      for (int i1 = 0; i1 < i_container; ++i1) {
        for (int j1 = 0; j1 < kRazmerArray; ++j1) {
          alloc_traits::destroy(alloc_, vec_[i1] + j1);
        }
      }
      for (int j1 = 0; j1 < j_container; ++j1) {
        alloc_traits::destroy(alloc_, vec_[i_container] + j1);
      }
      for (int i1 = size_left_ / kRazmerArray;
           i1 < capacity_ / kRazmerArray - size_right_ / kRazmerArray; ++i1) {
        alloc_traits::deallocate(alloc_, vec_[i1], kRazmerArray);
      }
      throw;
    }
  }

  Deque(int n, const Allocator& alloc = Allocator())
      : vec_(std::vector<T*>(
            ceil(static_cast<double>(n) / static_cast<double>(kRazmerArray)),
            nullptr)),
        size_left_(0),
        size_right_((kRazmerArray - n % kRazmerArray) % kRazmerArray),
        capacity_(
            ceil(static_cast<double>(n) / static_cast<double>(kRazmerArray)) *
            kRazmerArray),
        alloc_(alloc) {
    int i_container = 0;
    for (; i_container < capacity_ / kRazmerArray; ++i_container) {
      vec_[i_container] = alloc_traits::allocate(alloc_, kRazmerArray);
    }
    i_container = 0;
    int j_container = 0;
    try {
      if (0 == capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1) {
        for (int j = size_left_ % kRazmerArray;
             j < (kRazmerArray - size_right_ % kRazmerArray) % kRazmerArray;
             ++j) {
          j_container = j;
          alloc_traits::construct(alloc_, vec_[i_container] + j);
        }
      } else {
        for (; i_container <
               capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1;
             ++i_container) {
          for (int j = 0; j < kRazmerArray; ++j) {
            j_container = j;
            alloc_traits::construct(alloc_, vec_[i_container] + j);
          }
        }
        j_container = 0;
        for (int j = 0;
             j < (kRazmerArray - size_right_ % kRazmerArray) % kRazmerArray;
             ++j) {
          j_container = j;
          alloc_traits::construct(alloc_, vec_[i_container] + j);
        }
      }
    } catch (...) {
      for (int i1 = 0; i1 < i_container; ++i1) {
        for (int j1 = 0; j1 < kRazmerArray; ++j1) {
          alloc_traits::destroy(alloc_, vec_[i1] + j1);
        }
      }
      for (int j1 = 0; j1 < j_container; ++j1) {
        alloc_traits::destroy(alloc_, vec_[i_container] + j1);
      }
      for (int i1 = size_left_ / kRazmerArray;
           i1 < capacity_ / kRazmerArray - size_right_ / kRazmerArray; ++i1) {
        alloc_traits::deallocate(alloc_, vec_[i1], kRazmerArray);
      }
      throw;
    }
  }

  Deque(Deque<T, Allocator>&& other)
      : vec_(std::move(other.vec_)),
        size_left_(other.size_left_),
        size_right_(other.size_right_),
        capacity_(other.capacity_),
        alloc_(other.alloc_) {
    other.vec_.resize(3, nullptr);
    other.size_left_ = kRazmerArray;
    other.size_right_ = kRazmerArray * 2;
    other.capacity_ = kRazmerArray * 3;
  }

  Deque(std::initializer_list<T> init, const Allocator& alloc = Allocator())
      : vec_(std::vector<T*>(ceil(static_cast<double>(init.size()) /
                                  static_cast<double>(kRazmerArray)),
                             nullptr)),
        size_left_(0),
        size_right_((kRazmerArray - init.size() % kRazmerArray) % kRazmerArray),
        capacity_(ceil(static_cast<double>(init.size()) /
                       static_cast<double>(kRazmerArray)) *
                  kRazmerArray),
        alloc_(alloc) {
    int i_container = 0;
    for (; i_container < capacity_ / kRazmerArray; ++i_container) {
      vec_[i_container] = alloc_traits::allocate(alloc_, kRazmerArray);
    }
    i_container = 0;
    int j_container = 0;
    try {
      if (0 == capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1) {
        int count = 0;
        for (int j = size_left_ % kRazmerArray;
             j < (kRazmerArray - size_right_ % kRazmerArray) % kRazmerArray;
             ++j) {
          j_container = j;
          alloc_traits::construct(alloc_, vec_[i_container] + j,
                                  (std::move(*(init.begin() + count))));
          ++count;
        }
      } else {
        int count = 0;
        for (; i_container <
               capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1;
             ++i_container) {
          for (int j = 0; j < kRazmerArray; ++j) {
            j_container = j;
            alloc_traits::construct(alloc_, vec_[i_container] + j,
                                    (std::move(*(init.begin() + count))));
            ++count;
          }
        }
        j_container = 0;
        for (int j = 0;
             j < (kRazmerArray - size_right_ % kRazmerArray) % kRazmerArray;
             ++j) {
          j_container = j;
          alloc_traits::construct(alloc_, vec_[i_container] + j,
                                  (std::move(*(init.begin() + count))));
          ++count;
        }
      }
    } catch (...) {
      for (int i1 = 0; i1 < i_container; ++i1) {
        for (int j1 = 0; j1 < kRazmerArray; ++j1) {
          alloc_traits::destroy(alloc_, vec_[i1] + j1);
        }
      }
      for (int j1 = 0; j1 < j_container; ++j1) {
        alloc_traits::destroy(alloc_, vec_[i_container] + j1);
      }
      for (int i1 = size_left_ / kRazmerArray;
           i1 < capacity_ / kRazmerArray - size_right_ / kRazmerArray; ++i1) {
        alloc_traits::deallocate(alloc_, vec_[i1], kRazmerArray);
      }
      throw;
    }
  }

  ~Deque() {
    if (this->empty()) {
      return;
    }
    if (size_left_ / kRazmerArray ==
        capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1) {
      for (int j = size_left_ % kRazmerArray;
           j < (kRazmerArray - size_right_ % kRazmerArray) % kRazmerArray;
           ++j) {
        alloc_traits::destroy(alloc_, vec_[size_left_ / kRazmerArray] + j);
      }
      for (int i1 = size_left_ / kRazmerArray;
           i1 < capacity_ / kRazmerArray - size_right_ / kRazmerArray; ++i1) {
        alloc_traits::deallocate(alloc_, vec_[i1], kRazmerArray);
      }
    } else {
      for (int j = size_left_ % kRazmerArray; j < kRazmerArray; ++j) {
        alloc_traits::destroy(alloc_, vec_[size_left_ / kRazmerArray] + j);
      }
      for (int i = size_left_ / kRazmerArray + 1;
           i < capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1; ++i) {
        for (int j = 0; j < kRazmerArray; ++j) {
          alloc_traits::destroy(alloc_, vec_[i] + j);
        }
      }
      for (int j = 0;
           j < (kRazmerArray - size_right_ % kRazmerArray) % kRazmerArray;
           ++j) {
        alloc_traits::destroy(
            alloc_,
            vec_[capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1] +
                j);
      }
      for (int i1 = size_left_ / kRazmerArray;
           i1 < capacity_ / kRazmerArray - size_right_ / kRazmerArray; ++i1) {
        alloc_traits::deallocate(alloc_, vec_[i1], kRazmerArray);
      }
    }
  }

  Deque<T, Allocator>& operator=(const Deque<T, Allocator>& other) {
    if (std::allocator_traits<decltype(other.alloc_)>::
            propagate_on_container_copy_assignment::value) {
      alloc_ = other.alloc_;
    }
    std::vector<T*> vec_copy(other.vec_.size(), nullptr);
    int i_container = other.size_left_ / other.kRazmerArray;
    int j_container = other.size_left_ % other.kRazmerArray;
    insertion_ptr(vec_copy, other.size_left_ / other.kRazmerArray,
                  other.capacity_ / other.kRazmerArray -
                      other.size_right_ / other.kRazmerArray,
                  kRazmerArray);
    try {
      if (other.size_left_ / other.kRazmerArray ==
          other.capacity_ / other.kRazmerArray -
              other.size_right_ / other.kRazmerArray - 1) {
        for (; j_container <
               (kRazmerArray - other.size_right_ % kRazmerArray) % kRazmerArray;
             ++j_container) {
          alloc_traits::construct(
              alloc_,
              vec_copy[other.size_left_ / other.kRazmerArray] + j_container,
              other.vec_[other.size_left_ / other.kRazmerArray][j_container]);
        }
      } else {
        for (; j_container < other.kRazmerArray; ++j_container) {
          alloc_traits::construct(alloc_, vec_copy[i_container] + j_container,
                                  other.vec_[i_container][j_container]);
        }
        ++i_container;
        j_container = 0;
        for (; i_container < other.capacity_ / other.kRazmerArray -
                                 other.size_right_ / other.kRazmerArray - 1;
             ++i_container) {
          for (int j = 0; j < other.kRazmerArray; ++j) {
            j_container = j;
            alloc_traits::construct(alloc_, vec_copy[i_container] + j,
                                    other.vec_[i_container][j]);
          }
        }
        for (int j = 0;
             j < (kRazmerArray - size_right_ % kRazmerArray) % kRazmerArray;
             ++j) {
          j_container = j;
          alloc_traits::construct(alloc_, vec_copy[i_container] + j,
                                  other.vec_[i_container][j]);
        }
      }
    } catch (...) {  // catch exceptions from constructor of T
      uproshchenie(other, vec_copy, i_container, j_container);
      throw;
    }
    udalenie_arr(*this, vec_);
    vec_ = vec_copy;
    capacity_ = other.capacity_;
    size_left_ = other.size_left_;
    size_right_ = other.size_right_;
    return *this;
  }

  Deque& operator=(Deque&& other) {
    if (std::allocator_traits<decltype(other.alloc_)>::
            propagate_on_container_move_assignment::value) {
      alloc_ = std::move(other.alloc_);
    }
    vec_ = std::move(other.vec_);
    size_left_ = other.size_left_;
    size_right_ = other.size_right_;
    capacity_ = other.capacity_;
    other.vec_.resize(3, nullptr);
    other.size_left_ = kRazmerArray;
    other.size_right_ = kRazmerArray * 2;
    other.capacity_ = kRazmerArray * 3;
    return *this;
  }

  size_t size() const {
    return static_cast<size_t>(capacity_ - size_left_ - size_right_);
  }

  bool empty() const { return (capacity_ == (size_left_ + size_right_)); }

  T& operator[](size_t num) {
    int num_copy = static_cast<int>(num);
    return vec_[(size_left_ + num_copy) / kRazmerArray]
               [(size_left_ + num_copy) % kRazmerArray];
  }

  const T& operator[](size_t num) const {
    int num_copy = static_cast<int>(num);
    return vec_[(size_left_ + num_copy) / kRazmerArray]
               [(size_left_ + num_copy) % kRazmerArray];
  }

  T& at(size_t num) {
    int num_copy = static_cast<int>(num);
    if ((this->size() - 1) < num) {
      throw std::out_of_range("");
    }
    return (*this)[num_copy];
  }

  const T& at(size_t num) const {
    int num_copy = static_cast<int>(num);
    if ((this->size() - 1) < num) {
      throw std::out_of_range("");
    }
    return (*this)[num_copy];
  }

  void push_back(const T& value) {
    if (size_right_ % kRazmerArray != 0) {
      alloc_traits::construct(
          alloc_,
          vec_[capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1] +
              kRazmerArray - 1 - size_right_ % kRazmerArray + 1,
          value);
      --size_right_;
    } else {
      if (size_right_ != 0) {
        vec_[capacity_ / kRazmerArray - size_right_ / kRazmerArray] =
            alloc_traits::allocate(alloc_, kRazmerArray);
        try {
          alloc_traits::construct(
              alloc_,
              vec_[capacity_ / kRazmerArray - size_right_ / kRazmerArray],
              value);
          --size_right_;
        } catch (...) {  // catch exceptions from constructor of T
          alloc_traits::deallocate(
              alloc_,
              vec_[capacity_ / kRazmerArray - size_right_ / kRazmerArray],
              kRazmerArray);
          throw;
        }
      } else {
        std::vector<T*> vec_copy(
            3 * (capacity_ / kRazmerArray - size_left_ / kRazmerArray),
            nullptr);
        for (int i = capacity_ / kRazmerArray - size_left_ / kRazmerArray;
             i < 2 * (capacity_ / kRazmerArray - size_left_ / kRazmerArray);
             ++i) {
          vec_copy[i] =
              vec_[size_left_ / kRazmerArray + i - capacity_ / kRazmerArray +
                   size_left_ / kRazmerArray];
        }
        vec_ = vec_copy;
        vec_[2 * (capacity_ / kRazmerArray - size_left_ / kRazmerArray)] =
            alloc_traits::allocate(alloc_, kRazmerArray);
        try {
          alloc_traits::construct(
              alloc_,
              vec_[2 * (capacity_ / kRazmerArray - size_left_ / kRazmerArray)],
              value);
          this->swap_back();
        } catch (...) {  // same
          alloc_traits::deallocate(
              alloc_,
              vec_[2 * (capacity_ / kRazmerArray - size_left_ / kRazmerArray)],
              kRazmerArray);
          for (int i = size_left_ / kRazmerArray;
               i < capacity_ / kRazmerArray - size_right_ / kRazmerArray; ++i) {
            vec_[i] =
                vec_[capacity_ / kRazmerArray - 2 * size_left_ / kRazmerArray];
          }
          vec_.resize(capacity_ / kRazmerArray);
          throw;
        }
      }
    }
  }

  void push_back(T&& value) {
    if (size_right_ % kRazmerArray != 0) {
      alloc_traits::construct(
          alloc_,
          vec_[capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1] +
              kRazmerArray - 1 - size_right_ % kRazmerArray + 1,
          std::move(value));
      --size_right_;
    } else {
      if (size_right_ != 0) {
        vec_[capacity_ / kRazmerArray - size_right_ / kRazmerArray] =
            alloc_traits::allocate(alloc_, kRazmerArray);
        try {
          alloc_traits::construct(
              alloc_,
              vec_[capacity_ / kRazmerArray - size_right_ / kRazmerArray],
              std::move(value));
          --size_right_;
        } catch (...) {  // catch exceptions from constructor of T
          alloc_traits::deallocate(
              alloc_,
              vec_[capacity_ / kRazmerArray - size_right_ / kRazmerArray],
              kRazmerArray);
          throw;
        }
      } else {
        std::vector<T*> vec_copy(
            3 * (capacity_ / kRazmerArray - size_left_ / kRazmerArray),
            nullptr);
        for (int i = capacity_ / kRazmerArray - size_left_ / kRazmerArray;
             i < 2 * (capacity_ / kRazmerArray - size_left_ / kRazmerArray);
             ++i) {
          vec_copy[i] =
              vec_[size_left_ / kRazmerArray + i - capacity_ / kRazmerArray +
                   size_left_ / kRazmerArray];
        }
        vec_ = vec_copy;
        vec_[2 * (capacity_ / kRazmerArray - size_left_ / kRazmerArray)] =
            alloc_traits::allocate(alloc_, kRazmerArray);
        try {
          alloc_traits::construct(
              alloc_,
              vec_[2 * (capacity_ / kRazmerArray - size_left_ / kRazmerArray)],
              std::move(value));
          this->swap_back();
        } catch (...) {  // same
          alloc_traits::deallocate(
              alloc_,
              vec_[2 * (capacity_ / kRazmerArray - size_left_ / kRazmerArray)],
              kRazmerArray);
          for (int i = size_left_ / kRazmerArray;
               i < capacity_ / kRazmerArray - size_right_ / kRazmerArray; ++i) {
            vec_[i] =
                vec_[capacity_ / kRazmerArray - 2 * size_left_ / kRazmerArray];
          }
          vec_.resize(capacity_ / kRazmerArray);
          throw;
        }
      }
    }
  }

  template <typename... Args>
  void emplace_back(Args&&... args) {
    if (size_right_ % kRazmerArray != 0) {
      alloc_traits::construct(
          alloc_,
          vec_[capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1] +
              kRazmerArray - 1 - size_right_ % kRazmerArray + 1,
          std::forward<Args>(args)...);
      --size_right_;
    } else {
      if (size_right_ != 0) {
        vec_[capacity_ / kRazmerArray - size_right_ / kRazmerArray] =
            alloc_traits::allocate(alloc_, kRazmerArray);
        try {
          alloc_traits::construct(
              alloc_,
              vec_[capacity_ / kRazmerArray - size_right_ / kRazmerArray],
              std::forward<Args>(args)...);
          --size_right_;
        } catch (...) {  // catch exceptions from constructor of T
          alloc_traits::deallocate(
              alloc_,
              vec_[capacity_ / kRazmerArray - size_right_ / kRazmerArray],
              kRazmerArray);
          throw;
        }
      } else {
        std::vector<T*> vec_copy(
            3 * (capacity_ / kRazmerArray - size_left_ / kRazmerArray),
            nullptr);
        for (int i = capacity_ / kRazmerArray - size_left_ / kRazmerArray;
             i < 2 * (capacity_ / kRazmerArray - size_left_ / kRazmerArray);
             ++i) {
          vec_copy[i] =
              vec_[size_left_ / kRazmerArray + i - capacity_ / kRazmerArray +
                   size_left_ / kRazmerArray];
        }
        vec_ = vec_copy;
        vec_[2 * (capacity_ / kRazmerArray - size_left_ / kRazmerArray)] =
            alloc_traits::allocate(alloc_, kRazmerArray);
        try {
          alloc_traits::construct(
              alloc_,
              vec_[2 * (capacity_ / kRazmerArray - size_left_ / kRazmerArray)],
              std::forward<Args>(args)...);
          this->swap_back();
        } catch (...) {  // same
          alloc_traits::deallocate(
              alloc_,
              vec_[2 * (capacity_ / kRazmerArray - size_left_ / kRazmerArray)],
              kRazmerArray);
          for (int i = size_left_ / kRazmerArray;
               i < capacity_ / kRazmerArray - size_right_ / kRazmerArray; ++i) {
            vec_[i] =
                vec_[capacity_ / kRazmerArray - 2 * size_left_ / kRazmerArray];
          }
          vec_.resize(capacity_ / kRazmerArray);
          throw;
        }
      }
    }
  }

  void pop_back() {
    if (this->size() == 1) {
      alloc_traits::destroy(
          alloc_, vec_[size_left_ / kRazmerArray] + size_left_ % kRazmerArray);
      alloc_traits::deallocate(alloc_, vec_[size_left_ / kRazmerArray],
                               kRazmerArray);
      ++size_right_;
    } else {
      if (kRazmerArray - 1 - size_right_ % kRazmerArray == 0) {
        alloc_traits::destroy(
            alloc_,
            vec_[capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1] +
                kRazmerArray - 1 - size_right_ % kRazmerArray);
        alloc_traits::deallocate(
            alloc_,
            vec_[capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1],
            kRazmerArray);
        ++size_right_;
      } else {
        alloc_traits::destroy(
            alloc_,
            vec_[capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1] +
                kRazmerArray - 1 - size_right_ % kRazmerArray);
        ++size_right_;
      }
    }
  }

  void push_front(const T& value) {
    if (size_left_ % kRazmerArray != 0) {
      alloc_traits::construct(
          alloc_,
          vec_[size_left_ / kRazmerArray] + size_left_ % kRazmerArray - 1,
          value);
      --size_left_;
    } else {
      if (size_left_ != 0) {
        vec_[size_left_ / kRazmerArray - 1] =
            alloc_traits::allocate(alloc_, kRazmerArray);
        try {
          alloc_traits::construct(
              alloc_, vec_[size_left_ / kRazmerArray - 1] + kRazmerArray - 1,
              value);
          --size_left_;
        } catch (...) {  // catch exceptions from constructor of T
          alloc_traits::deallocate(alloc_, vec_[size_left_ / kRazmerArray - 1],
                                   kRazmerArray);
          throw;
        }
      } else {
        std::vector<T*> vec_copy(
            3 * (capacity_ / kRazmerArray - size_right_ / kRazmerArray),
            nullptr);
        for (int i = capacity_ / kRazmerArray - size_right_ / kRazmerArray;
             i < 2 * (capacity_ / kRazmerArray - size_right_ / kRazmerArray);
             ++i) {
          vec_copy[i] =
              vec_[i - capacity_ / kRazmerArray + size_right_ / kRazmerArray];
        }
        vec_ = vec_copy;
        vec_[capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1] =
            alloc_traits::allocate(alloc_, kRazmerArray);
        try {
          alloc_traits::construct(
              alloc_,
              vec_[capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1] +
                  kRazmerArray - 1,
              value);
          this->swap_front();
        } catch (...) {  // same
          alloc_traits::deallocate(
              alloc_,
              vec_[capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1],
              kRazmerArray);
          for (int i = 0;
               i < capacity_ / kRazmerArray - size_right_ / kRazmerArray; ++i) {
            vec_[i] =
                vec_[capacity_ / kRazmerArray - size_right_ / kRazmerArray + i];
          }
          vec_.resize(capacity_ / kRazmerArray);
          throw;
        }
      }
    }
  }

  void push_front(T&& value) {
    if (size_left_ % kRazmerArray != 0) {
      alloc_traits::construct(
          alloc_,
          vec_[size_left_ / kRazmerArray] + size_left_ % kRazmerArray - 1,
          std::move(value));
      --size_left_;
    } else {
      if (size_left_ != 0) {
        vec_[size_left_ / kRazmerArray - 1] =
            alloc_traits::allocate(alloc_, kRazmerArray);
        try {
          alloc_traits::construct(
              alloc_, vec_[size_left_ / kRazmerArray - 1] + kRazmerArray - 1,
              std::move(value));
          --size_left_;
        } catch (...) {  // catch exceptions from constructor of T
          alloc_traits::deallocate(alloc_, vec_[size_left_ / kRazmerArray - 1],
                                   kRazmerArray);
          throw;
        }
      } else {
        std::vector<T*> vec_copy(
            3 * (capacity_ / kRazmerArray - size_right_ / kRazmerArray),
            nullptr);
        for (int i = capacity_ / kRazmerArray - size_right_ / kRazmerArray;
             i < 2 * (capacity_ / kRazmerArray - size_right_ / kRazmerArray);
             ++i) {
          vec_copy[i] =
              vec_[i - capacity_ / kRazmerArray + size_right_ / kRazmerArray];
        }
        vec_ = vec_copy;
        vec_[capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1] =
            alloc_traits::allocate(alloc_, kRazmerArray);
        try {
          alloc_traits::construct(
              alloc_,
              vec_[capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1] +
                  kRazmerArray - 1,
              std::move(value));
          this->swap_front();
        } catch (...) {  // same
          alloc_traits::deallocate(
              alloc_,
              vec_[capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1],
              kRazmerArray);
          for (int i = 0;
               i < capacity_ / kRazmerArray - size_right_ / kRazmerArray; ++i) {
            vec_[i] =
                vec_[capacity_ / kRazmerArray - size_right_ / kRazmerArray + i];
          }
          vec_.resize(capacity_ / kRazmerArray);
          throw;
        }
      }
    }
  }

  template <typename... Args>
  void emplace_front(Args&&... args) {
    if (size_left_ % kRazmerArray != 0) {
      alloc_traits::construct(
          alloc_,
          vec_[size_left_ / kRazmerArray] + size_left_ % kRazmerArray - 1,
          std::forward<Args>(args)...);
      --size_left_;
    } else {
      if (size_left_ != 0) {
        vec_[size_left_ / kRazmerArray - 1] =
            alloc_traits::allocate(alloc_, kRazmerArray);
        try {
          alloc_traits::construct(
              alloc_, vec_[size_left_ / kRazmerArray - 1] + kRazmerArray - 1,
              std::forward<Args>(args)...);
          --size_left_;
        } catch (...) {  // catch exceptions from constructor of T
          alloc_traits::deallocate(alloc_, vec_[size_left_ / kRazmerArray - 1],
                                   kRazmerArray);
          throw;
        }
      } else {
        std::vector<T*> vec_copy(
            3 * (capacity_ / kRazmerArray - size_right_ / kRazmerArray),
            nullptr);
        for (int i = capacity_ / kRazmerArray - size_right_ / kRazmerArray;
             i < 2 * (capacity_ / kRazmerArray - size_right_ / kRazmerArray);
             ++i) {
          vec_copy[i] =
              vec_[i - capacity_ / kRazmerArray + size_right_ / kRazmerArray];
        }
        vec_ = vec_copy;
        vec_[capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1] =
            alloc_traits::allocate(alloc_, kRazmerArray);
        try {
          alloc_traits::construct(
              alloc_,
              vec_[capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1] +
                  kRazmerArray - 1,
              std::forward<Args>(args)...);
          this->swap_front();
        } catch (...) {  // same
          alloc_traits::deallocate(
              alloc_,
              vec_[capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1],
              kRazmerArray);
          for (int i = 0;
               i < capacity_ / kRazmerArray - size_right_ / kRazmerArray; ++i) {
            vec_[i] =
                vec_[capacity_ / kRazmerArray - size_right_ / kRazmerArray + i];
          }
          vec_.resize(capacity_ / kRazmerArray);
          throw;
        }
      }
    }
  }

  void swap_back() {
    int capacity_copy = 3 *
                        (capacity_ / kRazmerArray - size_left_ / kRazmerArray) *
                        kRazmerArray;
    int size_left_copy =
        size_left_ % kRazmerArray +
        (capacity_ / kRazmerArray - size_left_ / kRazmerArray) * kRazmerArray;
    int size_right_copy =
        (capacity_ / kRazmerArray - size_left_ / kRazmerArray) * kRazmerArray -
        1;
    capacity_ = capacity_copy;
    size_left_ = size_left_copy;
    size_right_ = size_right_copy;
  }

  void swap_front() {
    int capacity_copy =
        3 * (capacity_ / kRazmerArray - size_right_ / kRazmerArray) *
        kRazmerArray;
    int size_left_copy =
        (capacity_ / kRazmerArray - size_right_ / kRazmerArray) * kRazmerArray -
        1;
    int size_right_copy =
        (capacity_ / kRazmerArray - size_right_ / kRazmerArray) * kRazmerArray +
        size_right_ % kRazmerArray;
    capacity_ = capacity_copy;
    size_left_ = size_left_copy;
    size_right_ = size_right_copy;
  }

  void pop_front() {
    if (this->size() == 1) {
      alloc_traits::destroy(
          alloc_, vec_[size_left_ / kRazmerArray] + size_left_ % kRazmerArray);
      alloc_traits::deallocate(alloc_, vec_[size_left_ / kRazmerArray],
                               kRazmerArray);
      ++size_left_;
    } else {
      if (size_left_ % kRazmerArray == kRazmerArray - 1) {
        alloc_traits::destroy(
            alloc_, vec_[size_left_ / kRazmerArray] + kRazmerArray - 1);
        alloc_traits::deallocate(alloc_, vec_[size_left_ / kRazmerArray],
                                 kRazmerArray);
        ++size_left_;
      } else {
        alloc_traits::destroy(alloc_, vec_[size_left_ / kRazmerArray] +
                                          size_left_ % kRazmerArray);
        ++size_left_;
      }
    }
  }

  iterator begin() {
    return iterator(&vec_, size_left_ / kRazmerArray,
                    size_left_ % kRazmerArray);
  }

  const_iterator begin() const {
    return const_iterator(&vec_, size_left_ / kRazmerArray,
                          size_left_ % kRazmerArray);
  }

  const_iterator cbegin() const {
    return const_iterator(&vec_, size_left_ / kRazmerArray,
                          size_left_ % kRazmerArray);
  }

  iterator end() {
    return (size_right_ % kRazmerArray == 0)
               ? iterator(&vec_,
                          capacity_ / kRazmerArray - size_right_ / kRazmerArray,
                          0)
               : iterator(
                     &vec_,
                     capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1,
                     kRazmerArray - size_right_ % kRazmerArray);
  }

  const_iterator end() const {
    return (size_right_ % kRazmerArray == 0)
               ? const_iterator(
                     &vec_,
                     capacity_ / kRazmerArray - size_right_ / kRazmerArray, 0)
               : const_iterator(
                     &vec_,
                     capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1,
                     kRazmerArray - size_right_ % kRazmerArray);
  }

  const_iterator cend() const {
    return (size_right_ % kRazmerArray == 0)
               ? const_iterator(
                     &vec_,
                     capacity_ / kRazmerArray - size_right_ / kRazmerArray, 0)
               : const_iterator(
                     &vec_,
                     capacity_ / kRazmerArray - size_right_ / kRazmerArray - 1,
                     kRazmerArray - size_right_ % kRazmerArray);
  }

  reverse_iterator rbegin() { return std::make_reverse_iterator(this->end()); }

  const_reverse_iterator rbegin() const {
    return std::make_reverse_iterator(this->cend());
  }

  const_reverse_iterator crbegin() const {
    return std::make_reverse_iterator(this->cend());
  }

  reverse_iterator rend() { return std::make_reverse_iterator(this->begin()); }

  const_reverse_iterator rend() const {
    return std::make_reverse_iterator(this->begin());
  }

  const_reverse_iterator crend() {
    return std::make_reverse_iterator(this->begin());
  }

  void insert(iterator iter, const T& object) {
    Deque<T> deq;
    try {
      while ((iter - 1) != (this->end() - 1)) {
        deq.push_back(*(--this->end()));
        this->pop_back();
      }
      this->push_back(object);
      while (!deq.empty()) {
        this->push_back(*(--deq.end()));
        deq.pop_back();
      }
    } catch (...) {
      while (!deq.empty()) {
        this->push_back(*(--deq.end()));
        deq.pop_back();
      }
    }
  }

  template <typename... Args>
  void emplace(iterator iter, Args&&... args) {
    Deque<T> deq;
    try {
      while ((iter - 1) != (this->end() - 1)) {
        deq.push_back(*(--this->end()));
        this->pop_back();
      }
      this->emplace_back(args...);
      while (!deq.empty()) {
        this->push_back(*(--deq.end()));
        deq.pop_back();
      }
    } catch (...) {
      while (!deq.empty()) {
        this->push_back(*(--deq.end()));
        deq.pop_back();
      }
    }
  }

  void erase(iterator iter) {
    Deque<T> deq;
    try {
      while (iter != this->end() - 1) {
        deq.push_back(*(--this->end()));
        this->pop_back();
      }
      this->pop_back();
      while (!deq.empty()) {
        this->push_back(*(--deq.end()));
        deq.pop_back();
      }
    } catch (...) {
      while (!deq.empty()) {
        this->push_back(*(--deq.end()));
        deq.pop_back();
      }
    }
  }

  int get_capacity() const { return capacity_; }

  int get_size_left() const { return size_left_; }

  int get_size_right() const { return size_right_; }

  int get_razmer_of_array() const { return kRazmerArray; }

  Allocator get_allocator() const { return alloc_; }

  void destroy_elements(std::vector<T*>& vec, int position_i,
                        int position_j_begin, int position_j_end) {
    for (int j = position_j_begin; j < position_j_end; ++j) {
      alloc_traits::destroy(alloc_, vec[position_i] + j);
    }
  }

  void insertion_ptr(std::vector<T*>& vec, int position_i_begin,
                     int position_i_end, int k_razmer_array) {
    for (int i = position_i_begin; i < position_i_end; ++i) {
      vec[i] = alloc_traits::allocate(alloc_, k_razmer_array);
    }
  }

  void uproshchenie(const Deque<T, Allocator>& other, std::vector<T*>& vec,
                    int i_container, int j_container) {
    if (i_container == other.get_size_left() / other.get_razmer_of_array()) {
      destroy_elements(vec, i_container,
                       other.get_size_left() % other.get_razmer_of_array(),
                       j_container);
    } else {
      destroy_elements(vec, other.get_size_left() / other.get_razmer_of_array(),
                       other.get_size_left() % other.get_razmer_of_array(),
                       other.get_razmer_of_array());
      for (int i = other.get_size_left() / other.get_razmer_of_array() + 1;
           i < i_container; ++i) {
        destroy_elements(vec, i, 0, other.get_razmer_of_array());
      }
      destroy_elements(vec, i_container, 0, j_container);
    }
    for (int i = other.get_size_left() / other.get_razmer_of_array();
         i < other.get_capacity() / other.get_razmer_of_array() -
                 other.get_size_right() / other.get_razmer_of_array();
         ++i) {
      alloc_traits::deallocate(alloc_, vec[i], kRazmerArray);
    }
  }

  void udalenie_arr(Deque<T, Allocator>& deq, std::vector<T*>& vec) {
    if (deq.get_size_left() / deq.get_razmer_of_array() ==
        deq.get_capacity() / deq.get_razmer_of_array() -
            deq.get_size_right() / deq.get_razmer_of_array() - 1) {
      destroy_elements(vec, deq.get_size_left() / deq.get_razmer_of_array(),
                       deq.get_size_left() % deq.get_razmer_of_array(),
                       (deq.get_razmer_of_array() -
                        deq.get_size_right() % deq.get_razmer_of_array()) %
                           deq.get_razmer_of_array());
    } else {
      destroy_elements(vec, deq.get_size_left() / deq.get_razmer_of_array(),
                       deq.get_size_left() % deq.get_razmer_of_array(),
                       deq.get_razmer_of_array());
      for (int i = deq.get_size_left() / deq.get_razmer_of_array() + 1;
           i < deq.get_capacity() / deq.get_razmer_of_array() -
                   deq.get_size_right() / deq.get_razmer_of_array() - 1;
           ++i) {
        destroy_elements(vec, i, 0, deq.get_razmer_of_array());
      }
      destroy_elements(vec,
                       deq.get_capacity() / deq.get_razmer_of_array() -
                           deq.get_size_right() / deq.get_razmer_of_array() - 1,
                       0,
                       (deq.get_razmer_of_array() -
                        deq.get_size_right() % deq.get_razmer_of_array()) %
                           deq.get_razmer_of_array());
    }
    for (int i1 = deq.get_size_left() / deq.get_razmer_of_array();
         i1 < deq.get_capacity() / deq.get_razmer_of_array() -
                  deq.get_size_right() / deq.get_razmer_of_array();
         ++i1) {
      alloc_traits::deallocate(alloc_, vec[i1], kRazmerArray);
    }
  }

 private:
  const int kRazmerArray = 32;
  std::vector<T*> vec_;
  int size_left_;
  int size_right_;
  int capacity_;
  Allocator alloc_;
};

template <typename T, typename Allocator>
template <bool IsConst>
class Deque<T, Allocator>::common_iterator {
 public:
  using type = typename std::conditional<IsConst, const T, T>::type;
  using value_type = type;
  using iterator_category = std::random_access_iterator_tag;
  using pointer = type*;
  using reference = type&;
  using difference_type = std::ptrdiff_t;

  common_iterator(const std::vector<T*>* ptr, int num_i, int num_j)
      : ptr_(ptr), num_i_(num_i), num_j_(num_j) {}

  common_iterator<IsConst>& operator=(const common_iterator<IsConst>& iter) {
    ptr_ = iter.ptr_;
    num_i_ = iter.num_i_;
    num_j_ = iter.num_j_;
    return *this;
  }

  common_iterator(const common_iterator<IsConst>& iter) {
    ptr_ = iter.ptr_;
    num_i_ = iter.num_i_;
    num_j_ = iter.num_j_;
  }

  common_iterator<IsConst>& operator++() {
    if (num_j_ < kRazmerArray - 1) {
      ++num_j_;
    } else {
      ++num_i_;
      num_j_ = 0;
    }
    return *this;
  }

  common_iterator<IsConst>& operator--() {
    if (num_j_ != 0) {
      --num_j_;
    } else {
      --num_i_;
      num_j_ = kRazmerArray - 1;
    }
    return *this;
  }

  common_iterator<IsConst> operator+(int n) const {
    common_iterator<IsConst> iterator_answer(ptr_, num_i_, num_j_);
    iterator_answer.num_j_ += n % kRazmerArray;
    if (iterator_answer.num_j_ < 0) {
      iterator_answer.num_i_ += n / kRazmerArray - 1;
      iterator_answer.num_j_ += kRazmerArray;
    } else {
      if (iterator_answer.num_j_ > kRazmerArray - 1) {
        iterator_answer.num_i_ += n / kRazmerArray + 1;
        iterator_answer.num_j_ -= kRazmerArray;
      } else {
        iterator_answer.num_i_ += n / kRazmerArray;
      }
    }
    return iterator_answer;
  }

  common_iterator<IsConst> operator-(int n) const {
    common_iterator<IsConst> iterator_answer(ptr_, num_i_, num_j_);
    iterator_answer = iterator_answer + (-n);
    return iterator_answer;
  }

  template <bool IsCon>
  bool operator<(const common_iterator<IsCon>& iterator_second) const {
    bool answer;
    if (num_i_ != iterator_second.num_i_) {
      answer = (num_i_ < iterator_second.num_i_);
    } else {
      answer = (num_j_ < iterator_second.num_j_);
    }
    return answer;
  }

  template <bool IsCon>
  bool operator>(const common_iterator<IsCon>& iterator_second) const {
    return (iterator_second < *this);
  }

  template <bool IsCon>
  bool operator>=(const common_iterator<IsCon>& iterator_second) const {
    return !(*this < iterator_second);
  }

  template <bool IsCon>
  bool operator<=(const common_iterator<IsCon>& iterator_second) const {
    return !(iterator_second < *this);
  }

  template <bool IsCon>
  bool operator!=(const common_iterator<IsCon>& iterator_second) const {
    return (iterator_second < *this) || (*this < iterator_second);
  }

  template <bool IsCon>
  bool operator==(const common_iterator<IsCon>& iterator_second) const {
    return !((iterator_second < *this) || (*this < iterator_second));
  }

  operator common_iterator<true>() {
    return common_iterator<true>(ptr_, num_i_, num_j_);
  }

  reference operator*() const { return (*ptr_)[num_i_][num_j_]; }

  pointer operator->() const { return ((*ptr_)[num_i_] + num_j_); }

  std::ptrdiff_t operator-(const common_iterator& iterator_second) const {
    return (num_i_ - iterator_second.num_i_) * kRazmerArray +
           (num_j_ - iterator_second.num_j_);
  }

  common_iterator<IsConst> operator++(int) {
    common_iterator<IsConst> copy = *this;
    this->operator++();
    return copy;
  }

  common_iterator<IsConst>& operator+=(int n) {
    *this = *this + n;
    return *this;
  }

  common_iterator<IsConst> operator--(int) {
    common_iterator<IsConst> copy = *this;
    this->operator--();
    return copy;
  }

 private:
  const int kRazmerArray = 32;
  const std::vector<T*>* ptr_;
  int num_i_;
  int num_j_;
};