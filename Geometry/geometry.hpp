#pragma once

#include <wchar.h>

#include <algorithm>
#include <cmath>
#include <cstdint>

class Vector {
 public:
  Vector();
  Vector(int cox, int coy);
  int& GetX();
  int& GetY();
  const int& GetX() const;
  const int& GetY() const;
  Vector& operator+=(const Vector& vec);
  Vector& operator-=(const Vector& vec);
  Vector& operator*=(int chislo);

 private:
  int cox_ = 0;
  int coy_ = 0;
};

int operator*(const Vector& vec1, const Vector& vec2);

int operator^(const Vector& vec1, const Vector& vec2);

Vector operator+(const Vector& vec1, const Vector& vec2);

Vector operator-(const Vector& vec1, const Vector& vec2);

Vector operator*(const Vector& vec1, int chislo);

Vector operator*(int chislo, const Vector& vec1);

Vector operator-(const Vector& vec);

class Point;

class Segment;

class IShape {
 public:
  virtual void Move(const Vector& vec) = 0;
  virtual bool ContainsPoint(const Point& point) = 0;
  virtual bool CrossSegment(const Segment& seg) = 0;
  virtual IShape* Clone() = 0;
  virtual ~IShape() = default;
};

class Point : public IShape {
 public:
  Point();
  Point(int64_t cox, int64_t coy);
  int64_t& GetX();
  int64_t& GetY();
  const int64_t& GetX() const;
  const int64_t& GetY() const;
  void Move(const Vector& vec) override;
  bool ContainsPoint(const Point& point) override;
  bool CrossSegment(const Segment& seg) override;
  IShape* Clone() override;

 private:
  int64_t cox_ = 0;
  int64_t coy_ = 0;
};

class Segment : public IShape {
 public:
  Segment();
  Segment(const Point& point_a, const Point& point_b);
  Point& GetA();
  Point& GetB();
  const Point& GetA() const;
  const Point& GetB() const;
  void Move(const Vector& vec) override;
  bool ContainsPoint(const Point& point) override;
  bool CrossSegment(const Segment& seg) override;
  IShape* Clone() override;

 private:
  Point point_a_;
  Point point_b_;
};

class Line : public IShape {
 public:
  Line();
  Line(const Point& point_a, const Point& point_b);
  Line(int64_t coeff_a, int64_t coeff_b, int64_t coeff_c);
  int64_t& GetA();
  int64_t& GetB();
  int64_t& GetC();
  const int64_t& GetA() const;
  const int64_t& GetB() const;
  const int64_t& GetC() const;
  void Move(const Vector& vec) override;
  bool ContainsPoint(const Point& point) override;
  bool CrossSegment(const Segment& seg) override;
  IShape* Clone() override;

 private:
  int64_t coeff_a_;
  int64_t coeff_b_;
  int64_t coeff_c_;
};

class Ray : public IShape {
 public:
  Ray();
  Ray(const Point& point_a, const Point& point_b);
  Point& GetA();
  Vector& GetVector();
  const Point& GetA() const;
  const Vector& GetVector() const;
  void Move(const Vector& vec) override;
  bool ContainsPoint(const Point& point) override;
  bool CrossSegment(const Segment& seg) override;
  IShape* Clone() override;

 private:
  Point point_;
  Vector vec_;
};

class Circle : public IShape {
 public:
  Circle();
  Circle(const Point& point, size_t radius);
  Point& GetCentre();
  size_t& GetRadius();
  const Point& GetCentre() const;
  const size_t& GetRadius() const;
  void Move(const Vector& vec) override;
  bool ContainsPoint(const Point& point) override;
  bool CrossSegment(const Segment& seg) override;
  IShape* Clone() override;

 private:
  Point centre_;
  size_t radius_ = 0;
};

int64_t RasstVKv(const Point&, const Point&);

Vector operator-(const Point& poi1, const Point& poi2);

bool SegInDot(const Segment& seg);

Vector::Vector() : cox_(0), coy_(0) {}

Vector::Vector(int cox, int coy) : cox_(cox), coy_(coy) {}

int& Vector::GetX() { return (cox_); }

int& Vector::GetY() { return (coy_); }

const int& Vector::GetX() const { return (cox_); }

const int& Vector::GetY() const { return (coy_); }

int operator*(const Vector& vec1, const Vector& vec2) {
  return (vec1.GetX() * vec2.GetX() + vec1.GetY() * vec2.GetY());
}

int operator^(const Vector& vec1, const Vector& vec2) {
  return (vec1.GetX() * vec2.GetY() - vec1.GetY() * vec2.GetX());
}

Vector& Vector::operator+=(const Vector& vec) {
  cox_ += vec.GetX();
  coy_ += vec.GetY();
  return *this;
}

Vector& Vector::operator-=(const Vector& vec) {
  cox_ -= vec.GetX();
  coy_ -= vec.GetY();
  return *this;
}

Vector operator+(const Vector& vec1, const Vector& vec2) {
  Vector vec(vec1.GetX() + vec2.GetX(), vec1.GetY() + vec2.GetY());
  return vec;
}

Vector operator-(const Vector& vec1, const Vector& vec2) {
  Vector vec(vec1.GetX() - vec2.GetX(), vec1.GetY() - vec2.GetY());
  return vec;
}

Vector operator*(const Vector& vec1, int chislo) {
  Vector vec(vec1.GetX() * chislo, vec1.GetY() * chislo);
  return vec;
}

Vector operator*(int chislo, const Vector& vec1) {
  Vector vec(vec1.GetX() * chislo, vec1.GetY() * chislo);
  return vec;
}

Vector& Vector::operator*=(int chislo) {
  cox_ *= chislo;
  coy_ *= chislo;
  return *this;
}

Vector operator-(const Vector& vec) {
  Vector vec1(-vec.GetX(), -vec.GetY());
  return vec1;
}

Point::Point(int64_t cox, int64_t coy) : cox_(cox), coy_(coy) {}

Point::Point() : cox_(0), coy_(0) {}

int64_t& Point::GetX() { return (cox_); }

int64_t& Point::GetY() { return (coy_); }

const int64_t& Point::GetX() const { return (cox_); }

const int64_t& Point::GetY() const { return (coy_); }

Segment::Segment(const Point& point_a, const Point& point_b)
    : point_a_(point_a), point_b_(point_b) {}

Segment::Segment() {}

Point& Segment::GetA() { return point_a_; }
Point& Segment::GetB() { return point_b_; }
const Point& Segment::GetA() const { return point_a_; }
const Point& Segment::GetB() const { return point_b_; }

Line::Line() : coeff_a_(1), coeff_b_(1), coeff_c_(1) {}

Line::Line(const Point& point_a, const Point& point_b)
    : coeff_a_(point_b.GetY() - point_a.GetY()),
      coeff_b_(point_a.GetX() - point_b.GetX()),
      coeff_c_(point_a.GetY() * point_b.GetX() -
               point_a.GetX() * point_b.GetY()) {}
Line::Line(int64_t coeff_a, int64_t coeff_b, int64_t coeff_c)
    : coeff_a_(coeff_a), coeff_b_(coeff_b), coeff_c_(coeff_c) {}
int64_t& Line::GetA() { return coeff_a_; }
int64_t& Line::GetB() { return coeff_b_; }
int64_t& Line::GetC() { return coeff_c_; }
const int64_t& Line::GetA() const { return coeff_a_; }
const int64_t& Line::GetB() const { return coeff_b_; }
const int64_t& Line::GetC() const { return coeff_c_; }

Ray::Ray() : point_(Point(0, 0)), vec_(Vector(1, 0)) {}
Ray::Ray(const Point& point_a, const Point& point_b)
    : point_(point_a),
      vec_(Vector(point_b.GetX() - point_a.GetX(),
                  point_b.GetY() - point_a.GetY())) {}
Point& Ray::GetA() { return point_; }
Vector& Ray::GetVector() { return vec_; }
const Point& Ray::GetA() const { return point_; }
const Vector& Ray::GetVector() const { return vec_; }

Circle::Circle() {}
Circle::Circle(const Point& point, size_t radius)
    : centre_(point), radius_(radius) {}
Point& Circle::GetCentre() { return centre_; }
size_t& Circle::GetRadius() { return radius_; }
const Point& Circle::GetCentre() const { return centre_; }
const size_t& Circle::GetRadius() const { return radius_; }

void Point::Move(const Vector& vec) {
  cox_ += vec.GetX();
  coy_ += vec.GetY();
}

bool Point::ContainsPoint(const Point& point) {
  bool answer = (point.GetX() == cox_ && point.GetY() == coy_);
  return answer;
}

bool Point::CrossSegment(const Segment& seg) {
  Vector vec_ab(seg.GetB().GetX() - seg.GetA().GetX(),
                seg.GetB().GetY() - seg.GetA().GetY());
  Vector vec_ac(cox_ - seg.GetA().GetX(), coy_ - seg.GetA().GetY());
  Vector vec_bc(cox_ - seg.GetB().GetX(), coy_ - seg.GetB().GetY());
  bool answer = ((vec_ab ^ vec_ac) == 0) && ((vec_ab * vec_ac) >= 0) &&
                ((vec_bc * (-vec_ab)) >= 0);
  return answer;
}

IShape* Point::Clone() {
  IShape* pointer = new Point(cox_, coy_);
  return pointer;
}

void Line::Move(const Vector& vec) {
  coeff_c_ -= (vec.GetX() * coeff_a_ + vec.GetY() * coeff_b_);
}

bool Line::ContainsPoint(const Point& poin) {
  bool answer =
      (coeff_a_ * poin.GetX() + coeff_b_ * poin.GetY() + coeff_c_ == 0);
  return answer;
}

bool Line::CrossSegment(const Segment& seg) {
  bool answer = ((coeff_a_ * seg.GetA().GetX() + coeff_b_ * seg.GetA().GetY() +
                  coeff_c_) *
                     (coeff_a_ * seg.GetB().GetX() +
                      coeff_b_ * seg.GetB().GetY() + coeff_c_) <=
                 0);
  return answer;
}

IShape* Line::Clone() {
  IShape* pointer = new Line(coeff_a_, coeff_b_, coeff_c_);
  return pointer;
}

void Segment::Move(const Vector& vec) {
  point_a_.Move(vec);
  point_b_.Move(vec);
}

bool Segment::ContainsPoint(const Point& poin) {
  Point poin_non_const = poin;
  bool answer;
  if (point_a_.GetX() == point_b_.GetX() &&
      point_a_.GetY() == point_b_.GetY()) {
    answer = poin_non_const.ContainsPoint(point_a_);
  } else {
    answer = poin_non_const.CrossSegment(*this);
  }
  return answer;
}

bool Segment::CrossSegment(const Segment& seg) {
  Vector vec1(seg.GetB().GetX() - seg.GetA().GetX(),
              seg.GetB().GetY() - seg.GetA().GetY());
  Vector vec2(point_b_.GetX() - point_a_.GetX(),
              point_b_.GetY() - point_a_.GetY());
  bool answer;
  if ((vec1 ^ vec2) != 0) {
    Line line_first(seg.GetA(), seg.GetB());
    Line line_second(point_a_, point_b_);
    answer = (line_first.CrossSegment(*this) && line_second.CrossSegment(seg));
  } else {
    Vector vec3(point_a_.GetX() - seg.GetA().GetX(),
                point_a_.GetY() - seg.GetA().GetY());
    if ((vec1 ^ vec3) != 0) {
      answer = false;
    } else {
      Segment seg_copy(seg.GetA(), seg.GetB());
      answer =
          (seg_copy.ContainsPoint(point_a_) ||
           seg_copy.ContainsPoint(point_b_) ||
           this->ContainsPoint(seg.GetA()) || this->ContainsPoint(seg.GetB()));
    }
  }
  return answer;
}

IShape* Segment::Clone() {
  IShape* pointer = new Segment(point_a_, point_b_);
  return pointer;
}

void Ray::Move(const Vector& vec) { point_.Move(vec); }

bool Ray::ContainsPoint(const Point& point_cont) {
  Vector vec(point_cont.GetX() - point_.GetX(),
             point_cont.GetY() - point_.GetY());
  bool answer = ((vec ^ vec_) == 0 && (vec * vec_ >= 0));
  return answer;
}

bool Ray::CrossSegment(const Segment& seg) {
  Vector vec(seg.GetB().GetX() - seg.GetA().GetX(),
             seg.GetB().GetY() - seg.GetA().GetY());
  bool answer;
  if ((vec ^ vec_) != 0) {
    Point point_second(point_.GetX() + vec_.GetX(),
                       point_.GetY() + vec_.GetY());
    Line lin(point_, point_second);
    Vector vec1(seg.GetA().GetX() - point_.GetX(),
                seg.GetA().GetY() - point_.GetY());
    Vector vec2(seg.GetB().GetX() - point_.GetX(),
                seg.GetB().GetY() - point_.GetY());
    answer = false;
    if (lin.CrossSegment(seg)) {
      if (!(lin.ContainsPoint(seg.GetA()) || lin.ContainsPoint(seg.GetB()))) {
        if (vec1 * vec_ >= 0 || vec2 * vec_ >= 0) {
          answer = true;
        }
      } else {
        answer =
            this->ContainsPoint(seg.GetA()) || this->ContainsPoint(seg.GetB());
      }
    }
  } else {
    answer =
        (this->ContainsPoint(seg.GetA()) || this->ContainsPoint(seg.GetB()));
  }
  return answer;
}

IShape* Ray::Clone() {
  Point point(point_.GetX() + vec_.GetX(), point_.GetY() + vec_.GetY());
  IShape* pointer = new Ray(point_, point);
  return pointer;
}

int64_t RasstVKv(const Point& point1, const Point& point2) {
  int64_t answer =
      (point1.GetX() - point2.GetX()) * (point1.GetX() - point2.GetX()) +
      (point1.GetY() - point2.GetY()) * (point1.GetY() - point2.GetY());
  return answer;
}

void Circle::Move(const Vector& vec) { centre_.Move(vec); }

bool Circle::ContainsPoint(const Point& point) {
  bool answer =
      (static_cast<uint64_t>(RasstVKv(point, centre_)) <= radius_ * radius_);
  return answer;
}

bool Circle::CrossSegment(const Segment& seg) {
  int64_t radius_v_kv = radius_ * radius_;
  bool answer = false;
  if (!(RasstVKv(seg.GetA(), centre_) < radius_v_kv &&
        RasstVKv(seg.GetB(), centre_) < radius_v_kv)) {
    if (RasstVKv(seg.GetA(), centre_) > radius_v_kv &&
        RasstVKv(seg.GetB(), centre_) > radius_v_kv) {
      Line lin(seg.GetA(), seg.GetB());
      double chisl = pow((lin.GetA() * centre_.GetX() +
                          lin.GetB() * centre_.GetY() + lin.GetC()),
                         2);
      double znam = pow(lin.GetA(), 2) + pow(lin.GetB(), 2);
      if (chisl / znam <= static_cast<double>(radius_v_kv)) {
        answer = true;
      }
    } else {
      answer = true;
    }
  }
  return answer;
}

IShape* Circle::Clone() {
  IShape* pointer = new Circle(centre_, radius_);
  return pointer;
}

Vector operator-(const Point& poi1, const Point& poi2) {
  Vector vec(poi1.GetX() - poi2.GetX(), poi1.GetY() - poi2.GetY());
  return vec;
}

bool SegInDot(const Segment& seg) {
  bool answer = false;
  if (seg.GetA().GetX() == seg.GetB().GetX() &&
      seg.GetA().GetY() == seg.GetB().GetY()) {
    answer = true;
  }
  return answer;
}