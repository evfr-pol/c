#pragma once
#include <cmath>
#include <iostream>
#include <string>
#include <vector>

class BigInt {
 private:
  std::vector<int> chislo_ = {};
  bool znak_ = true;

 public:
  const int kBase = 1e9;
  const int kZifr = 9;
  const int kChar = 48;
  BigInt(std::string s);
  BigInt(int64_t n);
  BigInt(const BigInt& s);
  BigInt(BigInt&& s);
  BigInt() = default;
  BigInt& operator=(const BigInt&);
  BigInt& operator=(BigInt&&);
  const std::vector<int>& Chislo() const;
  const bool& Znak() const;
  bool& Znak();
  std::vector<int>& Chislo();
  BigInt& operator+=(const BigInt& s);
  BigInt& operator-=(const BigInt& s);
  BigInt& operator++();
  BigInt operator++(int);
  BigInt& operator--();
  BigInt operator--(int);
  BigInt& operator*=(const BigInt& s);
  BigInt& operator/=(const BigInt& s);
  BigInt& operator%=(const BigInt& s);
};

bool operator<(const BigInt&, const BigInt&);
bool operator>(const BigInt&, const BigInt&);
bool operator<=(const BigInt&, const BigInt&);
bool operator>=(const BigInt&, const BigInt&);
bool operator!=(const BigInt&, const BigInt&);
bool operator==(const BigInt&, const BigInt&);

BigInt operator-(const BigInt& s);

std::istream& operator>>(std::istream&, BigInt& s);

std::ostream& operator<<(std::ostream&, const BigInt& s);

bool SravneiePoModuluBolshe(const BigInt&, const BigInt&);

BigInt operator+(const BigInt& s1, const BigInt& s2);

BigInt operator-(const BigInt& s1, const BigInt& s2);

int StringVInt(const std::string& s);

void Izmenenie0(BigInt& s);

int ChisloCifrVChisle(int a);

void UdalenieOtrizatelnih(BigInt& s);

void OperatorPlusForDz(BigInt& s1, const BigInt& s2);

BigInt UmnozhenieNaZif(const BigInt& s, unsigned long long zif);

BigInt operator*(const BigInt& s1, const BigInt& s2);

void DelenieTwo(BigInt& s);

BigInt operator/(const BigInt& s1, const BigInt& s2);

BigInt operator%(const BigInt& s1, const BigInt& s2);

#include "big_integer.hpp"

BigInt::BigInt(std::string s) {
  if (s.size() == 2 && s[0] == '-' && s[1] == '0') {
    chislo_.push_back(0);
  } else {
    int vvod = 0;
    znak_ = !(s[0] == '-');
    if (s[0] == '-') {
      s = s.substr(1);
    }
    int index = s.size() - kZifr;
    std::string podstroka;
    while (index >= 0) {
      podstroka = s.substr(index, kZifr);
      vvod = StringVInt(podstroka);
      chislo_.push_back(vvod);
      index -= kZifr;
    }
    vvod = 0;
    int conv = static_cast<int>('0');
    for (size_t i = 0; i < s.size() % kZifr; ++i) {
      vvod *= kZifr + 1;
      vvod += static_cast<int>(s[i] - conv);
    }
    if (vvod != 0 || (s.size() == 1 && s[0] == '0')) {
      chislo_.push_back(vvod);
    }
  }
}

BigInt::BigInt(int64_t n) {
  if (n == 0) {
    chislo_.push_back(n);
  } else {
    znak_ = (n >= 0);
    uint64_t m = 0;
    m = (n >= 0) ? n : -n;
    while (m != 0) {
      int a = m % kBase;
      chislo_.push_back(a);
      m /= kBase;
    }
  }
}

BigInt::BigInt(const BigInt& s) : chislo_(s.Chislo()), znak_(s.Znak()) {}

BigInt::BigInt(BigInt&& s) : chislo_(std::move(s.Chislo())), znak_(s.Znak()) {
  s.Znak() = true;
}

BigInt& BigInt::operator=(const BigInt& s) {
  if (this == &s) {
    return *this;
  } else {
    chislo_ = s.Chislo();
    znak_ = s.Znak();
    return *this;
  }
}

BigInt& BigInt::operator=(BigInt&& s) {
  if (this == &s) {
    return *this;
  } else {
    chislo_ = std::move(s.Chislo());
    znak_ = s.Znak();
    s.Znak() = true;
    return *this;
  }
}

bool operator<(const BigInt& s1, const BigInt& s2) {
  bool answer = false;
  if (s1.Znak() != s2.Znak()) {
    answer = !s1.Znak();
  } else {
    if (s1.Chislo().size() != s2.Chislo().size()) {
      if (s1.Znak()) {
        answer = !(s1.Chislo().size() > s2.Chislo().size());
      } else {
        answer = (s1.Chislo().size() > s2.Chislo().size());
      }
    } else {
      if (s1.Znak()) {
        for (int i = s1.Chislo().size() - 1; i >= 0; --i) {
          if (s1.Chislo()[i] != s2.Chislo()[i]) {
            answer = !(s1.Chislo()[i] > s2.Chislo()[i]);
            break;
          }
        }
      } else {
        for (int i = s1.Chislo().size() - 1; i >= 0; --i) {
          if (s1.Chislo()[i] != s2.Chislo()[i]) {
            answer = (s1.Chislo()[i] > s2.Chislo()[i]);
          }
        }
      }
    }
  }
  return answer;
}

bool operator>(const BigInt& s1, const BigInt& s2) { return (s2 < s1); }

bool operator>=(const BigInt& s1, const BigInt& s2) { return !(s1 < s2); }

bool operator<=(const BigInt& s1, const BigInt& s2) { return !(s2 < s1); }

bool operator==(const BigInt& s1, const BigInt& s2) { return !(s1 != s2); }

bool operator!=(const BigInt& s1, const BigInt& s2) {
  return (s2 < s1 || s1 < s2);
}

BigInt operator-(const BigInt& s) {
  BigInt copy = s;
  if (s.Chislo().size() != 1 || s.Chislo()[0] != 0) {
    copy.Znak() = !(copy.Znak());
  } else {
    copy.Znak() = true;
  }
  return copy;
}

const std::vector<int>& BigInt::Chislo() const { return (chislo_); }

std::vector<int>& BigInt::Chislo() { return (chislo_); }

bool& BigInt::Znak() { return (znak_); }

const bool& BigInt::Znak() const { return (znak_); }

std::istream& operator>>(std::istream& is, BigInt& s1) {
  s1.Chislo().clear();
  std::string s;
  is >> s;
  int vvod = 0;
  if (s[0] == '-') {
    s1.Znak() = false;
    s = s.substr(1);
  } else {
    s1.Znak() = true;
  }
  int index = s.size() - s1.kZifr;
  std::string podstroka;
  while (index >= 0) {
    podstroka = s.substr(index, s1.kZifr);
    vvod = StringVInt(podstroka);
    s1.Chislo().push_back(vvod);
    index -= s1.kZifr;
  }
  vvod = 0;
  int conv = static_cast<int>('0');
  for (size_t i = 0; i < s.size() % s1.kZifr; ++i) {
    vvod *= 2 * 2 * 2 + 2;
    vvod += static_cast<int>(s[i] - conv);
  }
  if (vvod != 0 || (s.size() == 1 && s[0] == '0')) {
    s1.Chislo().push_back(vvod);
  }
  Izmenenie0(s1);
  return is;
}

std::ostream& operator<<(std::ostream& os, const BigInt& s) {
  if (!s.Znak()) {
    os << '-';
  }
  int chislo_cifr = 0;
  os << s.Chislo()[s.Chislo().size() - 1];
  for (int i = s.Chislo().size() - 2; i >= 0; --i) {
    chislo_cifr = ChisloCifrVChisle(s.Chislo()[i]);
    for (int j = 0; j < s.kZifr - chislo_cifr; ++j) {
      os << 0;
    }
    os << s.Chislo()[i];
  }
  return os;
}

BigInt& BigInt::operator+=(const BigInt& s) {
  if (znak_ == s.znak_) {
    chislo_.resize(std::max(s.Chislo().size(), chislo_.size()) + 2, 0);
    int sum = 0;
    for (size_t i = 0; i < s.Chislo().size(); ++i) {
      chislo_[i] += s.Chislo()[i];
    }
    for (size_t i = 0; i < chislo_.size() - 1; ++i) {
      int sum = chislo_[i];
      chislo_[i] = sum % kBase;
      chislo_[i + 1] += (sum - sum % kBase) / kBase;
    }
    while (chislo_[chislo_.size() - 1] == 0 && chislo_.size() > 1) {
      chislo_.pop_back();
    }
  } else {
    OperatorPlusForDz(*this, s);
  }
  return *this;
}

BigInt& BigInt::operator-=(const BigInt& s) {
  *this += -s;
  return *this;
}

BigInt& BigInt::operator++() {
  *this += 1;
  return *this;
}

BigInt BigInt::operator++(int) {
  BigInt copy = *this;
  *this += 1;
  return copy;
}

BigInt operator+(const BigInt& s1, const BigInt& s2) {
  BigInt copy = s1;
  copy += s2;
  return copy;
}

BigInt operator-(const BigInt& s1, const BigInt& s2) {
  BigInt copy = s1;
  copy += -s2;
  return copy;
}

BigInt& BigInt::operator--() {
  *this -= 1;
  return *this;
}

BigInt BigInt::operator--(int) {
  BigInt copy = *this;
  *this -= 1;
  return copy;
}

bool SravneiePoModuluBolshe(const BigInt& s1, const BigInt& s2) {
  bool answer = false;
  if (s1.Chislo().size() != s2.Chislo().size()) {
    answer = (s1.Chislo().size() > s2.Chislo().size());
  } else {
    for (int i = s2.Chislo().size() - 1; i >= 0; --i) {
      if (s1.Chislo()[i] != s2.Chislo()[i]) {
        answer = (s1.Chislo()[i] > s2.Chislo()[i]);
        break;
      }
    }
  }
  return answer;
}

void UdalenieOtrizatelnih(BigInt& s) {
  int iterator_for_non_null = 0;
  for (int i = s.Chislo().size() - 1; i >= 0; --i) {
    if (s.Chislo()[i] < 0) {
      iterator_for_non_null = i + 1;
      while (s.Chislo()[iterator_for_non_null] <= 0) {
        ++iterator_for_non_null;
      }
      --s.Chislo()[iterator_for_non_null];
      for (int j = iterator_for_non_null - 1; j > i; --j) {
        s.Chislo()[j] += s.kBase - 1;
      }
      s.Chislo()[i] += s.kBase;
    }
  }
}

void OperatorPlusForDz(BigInt& s1, const BigInt& s2) {
  if (SravneiePoModuluBolshe(s1, s2)) {
    for (int i = s2.Chislo().size() - 1; i >= 0; --i) {
      s1.Chislo()[i] -= s2.Chislo()[i];
    }
    UdalenieOtrizatelnih(s1);
    while (s1.Chislo()[s1.Chislo().size() - 1] == 0 && s1.Chislo().size() > 1) {
      s1.Chislo().pop_back();
    }
  } else {
    if (SravneiePoModuluBolshe(s2, s1)) {
      s1.Znak() = !(s1.Znak());
      s1.Chislo().resize(s2.Chislo().size(), 0);
      for (int i = s2.Chislo().size() - 1; i >= 0; --i) {
        s1.Chislo()[i] = s2.Chislo()[i] - s1.Chislo()[i];
      }
      UdalenieOtrizatelnih(s1);
      while (s1.Chislo()[s1.Chislo().size() - 1] == 0 &&
             s1.Chislo().size() > 1) {
        s1.Chislo().pop_back();
      }
    } else {
      s1.Znak() = true;
      s1.Chislo().clear();
      s1.Chislo().push_back(0);
    }
  }
}

int ChisloCifrVChisle(int a) {
  int answer = 0;
  if (a == 0) {
    answer = 1;
  } else {
    int dec = 2 * 2 * 2 + 2;
    while (a > 0) {
      ++answer;
      a /= dec;
    }
  }
  return answer;
}

int StringVInt(const std::string& s) {
  int answer = 0;
  int conv = static_cast<int>('0');
  int dec = 2 * 2 * 2 + 2;
  for (size_t i = 0; i < s.size(); ++i) {
    answer *= dec;
    answer += static_cast<int>(s[i] - conv);
  }
  return answer;
}

void Izmenenie0(BigInt& s) {
  if (s.Chislo().size() == 1 && s.Chislo()[0] == 0) {
    s.Znak() = true;
  }
}

BigInt UmnozhenieNaZif(const BigInt& s, unsigned long long zif) {
  BigInt result = 0;
  result.Chislo().resize(s.Chislo().size() + 1, 0);
  unsigned long long promezh = 0;
  for (size_t i = 0; i < s.Chislo().size(); ++i) {
    promezh = static_cast<unsigned long long>(s.Chislo()[i]) *
              static_cast<unsigned long long>(zif);
    result.Chislo()[i] += promezh % s.kBase;
    result.Chislo()[i + 1] +=
        (promezh / s.kBase + result.Chislo()[i] / s.kBase);
    result.Chislo()[i] %= s.kBase;
  }
  if (result.Chislo()[result.Chislo().size() - 1] == 0) {
    result.Chislo().pop_back();
  }
  return result;
}

BigInt& BigInt::operator*=(const BigInt& s) {
  if (s == 0 || *this == 0) {
    chislo_.clear();
    chislo_.push_back(0);
    znak_ = true;
  } else {
    znak_ = (znak_ == s.Znak());
    BigInt copy = *this;
    unsigned long long size = chislo_.size();
    chislo_.clear();
    chislo_.resize((s.Chislo().size() + 1) * (size + 1) + 1, 0);
    for (size_t i = 0; i < s.Chislo().size(); ++i) {
      BigInt result = UmnozhenieNaZif(copy, s.Chislo()[i]);
      for (size_t k = i; k < result.Chislo().size() + i; ++k) {
        chislo_[k] += result.Chislo()[k - i] % kBase;
        chislo_[k + 1] += result.Chislo()[k - i] / kBase + chislo_[k] / kBase;
        chislo_[k] %= kBase;
      }
    }
    while (chislo_.size() > 1 && chislo_[chislo_.size() - 1] == 0) {
      chislo_.pop_back();
    }
  }
  return *this;
}

BigInt operator*(const BigInt& s1, const BigInt& s2) {
  BigInt copy = s1;
  copy *= s2;
  return copy;
}

void DelenieTwo(BigInt& s) {
  BigInt copy = s;
  s.Chislo().clear();
  s.Chislo().resize(copy.Chislo().size(), 0);
  uint64_t mod = 0;
  uint64_t result;
  for (int i = copy.Chislo().size() - 1; i >= 0; --i) {
    result = (mod * static_cast<uint64_t>(s.kBase) +
              static_cast<uint64_t>(copy.Chislo()[i])) /
             2ULL;
    s.Chislo()[i] = result;
    if (copy.Chislo()[i] % 2 == 0) {
      mod = 0;
    } else {
      mod = 1;
    }
  }
  while (s.Chislo()[s.Chislo().size() - 1] == 0 && s.Chislo().size() > 1) {
    s.Chislo().pop_back();
  }
}

BigInt& BigInt::operator/=(const BigInt& s) {
  BigInt begin = 0;
  bool znak_copy_this = znak_;
  znak_ = begin.Znak();
  BigInt end = *this;
  BigInt promezh = 0;
  while (end - begin > 1) {
    promezh = begin + end;
    DelenieTwo(promezh);
    if (SravneiePoModuluBolshe(promezh * s, *this)) {
      end = promezh;
    } else {
      begin = promezh;
    }
  }
  *this = (!SravneiePoModuluBolshe(end * s, *this)) ? end : begin;
  znak_ = (znak_copy_this == s.Znak());
  Izmenenie0(*this);
  return *this;
}

BigInt operator/(const BigInt& s1, const BigInt& s2) {
  BigInt copy = s1;
  copy /= s2;
  return copy;
}

BigInt& BigInt::operator%=(const BigInt& s) {
  *this = *this - (*this / s) * s;
  return *this;
}

BigInt operator%(const BigInt& s1, const BigInt& s2) {
  BigInt copy = s1;
  copy %= s2;
  return copy;
}