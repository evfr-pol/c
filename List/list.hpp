#pragma once
#include <initializer_list>
#include <iostream>
#include <iterator>
#include <memory>

template <typename T, typename Allocator = std::allocator<T>>
class List {
 private:
  struct BaseNode {
   public:
    BaseNode() : prev(nullptr), next(nullptr) {}
    BaseNode(BaseNode* prev, BaseNode* next) : prev(prev), next(next) {}
    BaseNode(const BaseNode& other) : prev(other.prev), next(other.next) {}
    BaseNode& operator=(const BaseNode& other) {
      prev = other.prev;
      next = other.next;
      return *this;
    }
    BaseNode* prev;
    BaseNode* next;
  };

  struct Node : public BaseNode {
   public:
    Node() {}
    Node(const T& value) : BaseNode(), value(value) {}
    Node(BaseNode* prev, BaseNode* next, const T& value)
        : BaseNode(prev, next), value(value) {}
    Node(const Node& other)
        : BaseNode(other.prev, other.next), value(other.value) {}
    Node& operator=(const Node& other) {
      this->prev = other.prev;
      this->next = other.next;
      value = other.value;
      return *this;
    }
    T value;
  };

 public:
  template <bool IsConst>
  class common_iter;

  using value_type = T;
  using allocator_type = Allocator;
  using alloc_traits = std::allocator_traits<Allocator>;
  using node_alloc = typename alloc_traits::template rebind_alloc<Node>;
  using node_alloc_traits = std::allocator_traits<node_alloc>;
  using iterator = common_iter<false>;
  using const_iterator = common_iter<true>;
  using reverse_iterator = std::reverse_iterator<iterator>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;

  List() : fake_(BaseNode(nullptr, nullptr)), size_(0), alloc_(node_alloc()) {}

  List(size_t count, const T& value, const Allocator& alloc = Allocator())
      : fake_(BaseNode(nullptr, nullptr)), size_(count), alloc_(alloc) {
    size_t index = 0;
    BaseNode* pred = &fake_;
    Node* node;
    try {
      for (; index < count; ++index) {
        node = node_alloc_traits::allocate(alloc_, 1);
        node_alloc_traits::construct(alloc_, node, value);
        pred->next = node;
        node->prev = pred;
        pred = node;
      }
      fake_.prev = pred;
      pred->next = &fake_;
    } catch (...) {
      node_alloc_traits::deallocate(alloc_, node, 1);
      for (; index > 0; --index) {
        BaseNode* copy = pred->prev;
        node_alloc_traits::destroy(alloc_, static_cast<Node*>(pred));
        node_alloc_traits::deallocate(alloc_, static_cast<Node*>(pred), 1);
        pred = copy;
      }
      size_ = 0;
      fake_ = BaseNode(nullptr, nullptr);
      throw;
    }
  }

  explicit List(size_t count, const Allocator& alloc = Allocator())
      : fake_(BaseNode(nullptr, nullptr)), size_(count), alloc_(alloc) {
    size_t index = 0;
    BaseNode* pred = &fake_;
    Node* node;
    try {
      for (; index < count; ++index) {
        node = node_alloc_traits::allocate(alloc_, 1);
        node_alloc_traits::construct(alloc_, node);
        pred->next = node;
        node->prev = pred;
        pred = node;
      }
      fake_.prev = pred;
      pred->next = &fake_;
    } catch (...) {
      node_alloc_traits::deallocate(alloc_, node, 1);
      for (; index > 0; --index) {
        BaseNode* copy = pred->prev;
        node_alloc_traits::destroy(alloc_, static_cast<Node*>(pred));
        node_alloc_traits::deallocate(alloc_, static_cast<Node*>(pred), 1);
        pred = copy;
      }
      size_ = 0;
      fake_ = BaseNode(nullptr, nullptr);
      throw;
    }
  }

  List(const List& other)
      : fake_(BaseNode(nullptr, nullptr)),
        size_(other.size_),
        alloc_(std::allocator_traits<decltype(other.alloc_)>::
                   select_on_container_copy_construction(other.alloc_)) {
    size_t index = 0;
    BaseNode* pred = &fake_;
    BaseNode* teku = other.fake_.next;
    Node* node;
    try {
      for (; index < other.size_; ++index) {
        node = node_alloc_traits::allocate(alloc_, 1);
        node_alloc_traits::construct(alloc_, node,
                                     static_cast<Node*>(teku)->value);
        pred->next = node;
        node->prev = pred;
        pred = node;
        teku = teku->next;
      }
      fake_.prev = pred;
      pred->next = &fake_;
    } catch (...) {
      node_alloc_traits::deallocate(alloc_, node, 1);
      for (; index > 0; --index) {
        BaseNode* copy = pred->prev;
        node_alloc_traits::destroy(alloc_, static_cast<Node*>(pred));
        node_alloc_traits::deallocate(alloc_, static_cast<Node*>(pred), 1);
        pred = copy;
      }
      size_ = 0;
      fake_ = BaseNode(nullptr, nullptr);
      throw;
    }
  }

  List(std::initializer_list<T> init, const Allocator& alloc = Allocator())
      : fake_(BaseNode(nullptr, nullptr)), size_(init.size()), alloc_(alloc) {
    size_t index = 0;
    BaseNode* pred = &fake_;
    Node* node;
    try {
      for (; index < init.size(); ++index) {
        node = node_alloc_traits::allocate(alloc_, 1);
        node_alloc_traits::construct(alloc_, node, *(init.begin() + index));
        pred->next = node;
        node->prev = pred;
        pred = node;
      }
      fake_.prev = pred;
      pred->next = &fake_;
    } catch (...) {
      node_alloc_traits::deallocate(alloc_, node, 1);
      for (; index > 0; --index) {
        BaseNode* copy = pred->prev;
        node_alloc_traits::destroy(alloc_, static_cast<Node*>(pred));
        node_alloc_traits::deallocate(alloc_, static_cast<Node*>(pred), 1);
        pred = copy;
      }
      size_ = 0;
      fake_ = BaseNode(nullptr, nullptr);
      throw;
    }
  }

  List(const List& other, const Allocator& alloc)
      : fake_(BaseNode(nullptr, nullptr)), size_(other.size_), alloc_(alloc) {
    size_t index = 0;
    BaseNode* pred = &fake_;
    BaseNode* teku = other.fake_.next;
    Node* node;
    try {
      for (; index < other.size_; ++index) {
        node = node_alloc_traits::allocate(alloc_, 1);
        node_alloc_traits::construct(alloc_, node,
                                     static_cast<Node*>(teku)->value);
        pred->next = node;
        node->prev = pred;
        pred = node;
        teku = teku->next;
      }
      fake_.prev = pred;
      pred->next = &fake_;
    } catch (...) {
      node_alloc_traits::deallocate(alloc_, node, 1);
      for (; index > 0; --index) {
        BaseNode* copy = pred->prev;
        node_alloc_traits::destroy(alloc_, static_cast<Node*>(pred));
        node_alloc_traits::deallocate(alloc_, static_cast<Node*>(pred), 1);
        pred = copy;
      }
      size_ = 0;
      fake_ = BaseNode(nullptr, nullptr);
      throw;
    }
  }

  ~List() {
    BaseNode* pred = fake_.prev;
    for (size_t index = size_; index > 0; --index) {
      BaseNode* copy = pred->prev;
      node_alloc_traits::destroy(alloc_, static_cast<Node*>(pred));
      node_alloc_traits::deallocate(alloc_, static_cast<Node*>(pred), 1);
      pred = copy;
    }
  }

  List& operator=(const List& other) {
    if (alloc_traits::propagate_on_container_copy_assignment::value) {
      List<T, Allocator> copy(other, other.alloc_);
      this->swap(copy);
    } else {
      List<T, Allocator> copy(other, other.alloc_);
      this->swap_without_alloc(copy);
    }
    return *this;
  }

  void swap(List& other) {
    std::swap(fake_, other.fake_);
    std::swap(size_, other.size_);
    std::swap(alloc_, other.alloc_);
  }

  void swap_without_alloc(List& other) {
    std::swap(fake_, other.fake_);
    std::swap(size_, other.size_);
  }

  iterator begin() { return iterator(fake_.next); }

  const_iterator begin() const { return const_iterator(fake_.next); }

  const_iterator cbegin() const { return const_iterator(fake_.next); }

  iterator end() { return iterator(&fake_); }

  const_iterator end() const { return const_iterator(&fake_); }

  const_iterator cend() const { return const_iterator(&fake_); }

  reverse_iterator rbegin() { return std::make_reverse_iterator(this->end()); }

  const_reverse_iterator rbegin() const {
    return std::make_reverse_iterator(this->cend());
  }

  const_reverse_iterator crbegin() const {
    return std::make_reverse_iterator(this->cend());
  }

  reverse_iterator rend() { return std::make_reverse_iterator(this->begin()); }

  const_reverse_iterator rend() const {
    return std::make_reverse_iterator(this->begin());
  }

  const_reverse_iterator crend() {
    return std::make_reverse_iterator(this->begin());
  }

  T& front() { return *(this->begin()); }

  const T& front() const { return *(this->begin()); }

  T& back() { return *(--(this->begin())); }

  const T& back() const { return *(--(this->begin())); }

  bool empty() const { return (size_ == 0); }

  size_t size() const { return size_; }

  void push_back(const T& object) {
    Node* node = node_alloc_traits::allocate(alloc_, 1);
    try {
      node_alloc_traits::construct(alloc_, node, object);
      if (size_ == 0) {
        fake_.prev = node;
        fake_.next = node;
        node->prev = &fake_;
        node->next = &fake_;
        ++size_;
      } else {
        BaseNode* copy = fake_.prev;
        copy->next = node;
        fake_.prev = node;
        node->prev = copy;
        node->next = &fake_;
        ++size_;
      }
    } catch (...) {
      node_alloc_traits::deallocate(alloc_, node, 1);
    }
  }

  void push_back(T&& object) {
    Node* node = node_alloc_traits::allocate(alloc_, 1);
    try {
      node_alloc_traits::construct(alloc_, node, std::move(object));
      if (size_ == 0) {
        fake_.prev = node;
        fake_.next = node;
        node->prev = &fake_;
        node->next = &fake_;
        ++size_;
      } else {
        BaseNode* copy = fake_.prev;
        copy->next = node;
        fake_.prev = node;
        node->prev = copy;
        node->next = &fake_;
        ++size_;
      }
    } catch (...) {
      node_alloc_traits::deallocate(alloc_, node, 1);
    }
  }

  void push_front(const T& object) {
    Node* node = node_alloc_traits::allocate(alloc_, 1);
    try {
      node_alloc_traits::construct(alloc_, node, object);
      if (size_ == 0) {
        fake_.prev = node;
        fake_.next = node;
        node->prev = &fake_;
        node->next = &fake_;
        ++size_;
      } else {
        BaseNode* copy = fake_.next;
        copy->prev = node;
        fake_.next = node;
        node->prev = &fake_;
        node->next = copy;
        ++size_;
      }
    } catch (...) {
      node_alloc_traits::deallocate(alloc_, node, 1);
    }
  }

  void push_front(T&& object) {
    Node* node = node_alloc_traits::allocate(alloc_, 1);
    try {
      node_alloc_traits::construct(alloc_, node, std::move(object));
      if (size_ == 0) {
        fake_.prev = node;
        fake_.next = node;
        node->prev = &fake_;
        node->next = &fake_;
        ++size_;
      } else {
        BaseNode* copy = fake_.next;
        copy->prev = node;
        fake_.next = node;
        node->prev = &fake_;
        node->next = copy;
        ++size_;
      }
    } catch (...) {
      node_alloc_traits::deallocate(alloc_, node, 1);
    }
  }

  void pop_back() {
    Node* node_udalenie = static_cast<Node*>(fake_.prev);
    Node* prev_node_udalenie = static_cast<Node*>(node_udalenie->prev);
    node_alloc_traits::destroy(alloc_, node_udalenie);
    node_alloc_traits::deallocate(alloc_, node_udalenie, 1);
    prev_node_udalenie->next = &fake_;
    fake_.prev = prev_node_udalenie;
    --size_;
  }

  void pop_front() {
    Node* node_udalenie = static_cast<Node*>(fake_.next);
    Node* next_node_udalenie = static_cast<Node*>(node_udalenie->next);
    node_alloc_traits::destroy(alloc_, node_udalenie);
    node_alloc_traits::deallocate(alloc_, node_udalenie, 1);
    next_node_udalenie->prev = &fake_;
    fake_.next = next_node_udalenie;
    --size_;
  }

  Allocator get_allocator() { return alloc_; }

 private:
  BaseNode fake_;
  size_t size_;
  node_alloc alloc_;
};

template <typename T, typename Allocator>
template <bool IsConst>
class List<T, Allocator>::common_iter {
 public:
  using value_type = std::conditional_t<IsConst, const T, T>;
  using pointer = value_type*;
  using reference = value_type&;
  using iterator_category = std::bidirectional_iterator_tag;
  using difference_type = std::ptrdiff_t;
  using base_pointer_type =
      std::conditional_t<IsConst, const BaseNode*, BaseNode*>;
  using node_pointer_type = std::conditional_t<IsConst, const Node*, Node*>;

  common_iter(base_pointer_type ptr) : ptr_(ptr) {}

  common_iter(const common_iter<IsConst>& other) = default;

  common_iter<IsConst>& operator=(const common_iter<IsConst>& other) = default;

  operator common_iter<true>() const { return common_iter<true>(ptr_); }

  template <bool IsConstOther>
  bool operator==(const common_iter<IsConstOther>& other) const {
    return (ptr_ == other.ptr_);
  }

  template <bool IsConstOther>
  bool operator!=(const common_iter<IsConstOther>& other) const {
    return !(ptr_ == other.ptr_);
  }

  reference operator*() const {
    return static_cast<node_pointer_type>(ptr_)->value;
  }

  pointer operator->() const {
    return &(static_cast<node_pointer_type>(ptr_)->value);
  }

  common_iter<IsConst>& operator++() {
    ptr_ = ptr_->next;
    return *this;
  }

  common_iter<IsConst> operator++(int) {
    common_iter<IsConst> copy = *this;
    this->operator++();
    return copy;
  }

  common_iter<IsConst>& operator--() {
    ptr_ = ptr_->prev;
    return *this;
  }

  common_iter<IsConst> operator--(int) {
    common_iter<IsConst> copy = *this;
    this->operator--();
    return copy;
  }

 private:
  base_pointer_type ptr_;
};